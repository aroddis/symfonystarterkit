#!/usr/bin/env bash
echo "Removing Starterkit-Git-Files!"
rm -rf ./.git/
echo ""
read -p "Enter project name: "  projectName
read -p "Enter Vhost (e.g yourproject.local): "  vhost

sed -i "s/GiffitsStarterKit/$projectName/g" ./Vagrantfile
sed -i "s/symfony\.local/$vhost/g" ./infrastructure/containers/apache/config/vhost/symfony.conf
sed -i "s/symfony\.local/$vhost/g" ./infrastructure/scripts/console.sh
sed -i "s/192\.168\.33\.10/$vhost/g" ./infrastructure/scripts/message.txt

echo "Project '$projectName' setup correctly! Vhost: $vhost"
echo ""
echo "Starting build process..."
echo ""
sleep 2s
vagrant up