#!/usr/bin/env bash

export COMPOSE_INTERACTIVE_NO_CLI=1

COMPOSER_COMMAND="$*"

cd /var/www

docker-compose run -w /var/www/app --rm composer composer $COMPOSER_COMMAND

echo "Removing stale VendorFiles..."

docker-compose exec php rm -rf /php-vendor

echo "Moving updated files to Container..."

docker cp /var/www/app/vendor php:/php-vendor

echo "Executing ComposerScripts for Symfony - cache:clear"

docker-compose exec php /var/www/app/bin/console cache:clear

echo "Executing ComposerScripts for Symfony - assets:install"

docker-compose exec php /var/www/app/bin/console assets:install /var/www/app/public
