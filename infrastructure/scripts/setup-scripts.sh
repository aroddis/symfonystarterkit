#!/usr/bin/env bash

if [ -f /usr/bin/composer ]; then
  rm /usr/bin/composer
fi

cp /vagrant/infrastructure/scripts/composer.sh /usr/bin/composer

chmod a+x /usr/bin/composer

if [ -f /usr/bin/console ]; then
  rm /usr/bin/console
fi

cp /vagrant/infrastructure/scripts/console.sh /usr/bin/console

chmod a+x /usr/bin/console