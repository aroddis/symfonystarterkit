################################## Base PHP dependencies ##################################

FROM php:7.3-fpm-stretch as base

ARG DEBIAN_FRONTEND=noninteractive

ENV TZ Europe/Berlin

RUN buildDeps=" \
        default-libmysqlclient-dev \
        libbz2-dev \
        libmemcached-dev \
        libsasl2-dev \
    " \
    runtimeDeps=" \
        curl \
        openssl \
        libcurl3 \
        libcurl3-dev \
        libfreetype6-dev \
        libicu-dev \
        librabbitmq-dev \
        libssh-dev \
        libpq-dev \
        libxml2-dev \
    " \
    && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends $buildDeps $runtimeDeps \
    && docker-php-ext-install bcmath bz2 calendar iconv intl mbstring mysqli opcache pdo_mysql soap curl \
    && docker-php-ext-install exif \
    && pecl install redis amqp \
    && docker-php-ext-enable redis.so amqp \
    && apt-get purge -y --auto-remove $buildDeps \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone \
    && echo "date.timezone = \"Europe/Berlin\"" >> /usr/local/etc/php/conf.d/docker-php-timezone.ini \
    && rm -rf /var/lib/apt/lists/*

################################## Composer Base ##################################

FROM base as composer

RUN apt-get update && apt-get install -y zlib1g-dev libzip-dev git zip unzip

RUN docker-php-ext-install zip

RUN rm -rf /var/lib/apt/lists/*

RUN curl --silent --show-error https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

################################## Composer PHP dependencies ##################################

FROM composer as vendor

COPY ./app /var/www/app

################################## Frontend dependencies ##################################

FROM node:8.11 as frontend

RUN mkdir -p /app/public

#COPY ./app/package.json ./app/webpack.mix.js ./app/yarn.lock /app/

################################## Production Build ##################################

FROM base as production

COPY ./app /var/www/app

################################## Development Build ##################################

FROM base as development

RUN apt-get update \
    && apt-get install --no-install-recommends --assume-yes --quiet ca-certificates curl git \
    && pecl install xdebug-2.7.2 && docker-php-ext-enable xdebug \
    && echo 'xdebug.remote_port=9000' >> /usr/local/etc/php/php.ini \
    && echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/php.ini \
    && echo 'xdebug.remote_connect_back=1' >> /usr/local/etc/php/php.ini \
    && rm -rf /var/lib/apt/lists/*

COPY ./app /var/www/app