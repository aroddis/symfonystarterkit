### **Giffits Microservice Starterkit**

---
#### **Features:**

- DockerSwarm
- Portainer
- MultiStage DockerBuilds
- "**Ready to go**"-Microservice Projekt von Symfony
- Docker-Image Scanner: [Dive](https://github.com/wagoodman/dive)
- Api-Mocking: [Mock-Server](http://www.mock-server.com/)
  

#### Get Started

1. Clone this Repository
2. Run **./init.sh** inside the main dir in a **git-bash**!


#### Using composer

There is a shell-script inside the vagrant-box, that eases the use of composer inside your project.   
It will take care of running composer in the appropriate containers, copy the files into the right places    
and is to use just like composer.

Just type **composer require <vendorName/libName>** or every other command you are used to from composer.

#### Using console

There is a console script, that takes care of using the right container, starting the xdebug-connection to the host-ip
and piping output to your screen.

Just type **console <symfony-console:command>**.    